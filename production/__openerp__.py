# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Production',
    'version': '1',
    'summary': 'Quality Assurance',
    'sequence': 30,
    'category': 'MRP',
    'depends': [
        'quality_assurance', 'mrp_operations'
    ],
    'data': [
        'data/mrp_production_data.xml',
        'views/mrp_views.xml',
        'wizard/workcenter_line_input_views.xml',
        'wizard/quality_verification_workorder_views.xml',
        'wizard/mrp_produce_views.xml',
        'wizard/production_inventory_views.xml',
        'wizard/production_status_views.xml',
        'report/report_mrp_inventory_templates.xml',
        'report/report_mrp_production_templates.xml',
        'report/report_production_status_templates.xml',
    ],
    'demo': [
    ],
    'installable': True,
}
