# -*- coding: utf-8 -*-

from openerp import api, models


class ProductionInventory(models.AbstractModel):
    _name = 'report.production.report_production_status'
    _description = 'Report Production Status'

    @api.multi
    def render_html(self, data):
        domain = []  # [('state', '=', 'done')]

        states = []
        if data['all_new']:
            states += ['new']
        if data['all_cancel']:
            states += ['cancel']
        if data['all_confirmed']:
            states += ['confirmed']
        if data['all_ready']:
            states += ['ready']
        if data['all_in_production']:
            states += ['in_production']
        if data['all_done']:
            states += ['done']

        if data['all_batch']:
            states = []

        domain = []
        if states:
            domain += [('state', 'in', states)]

        # Start - End DATE
        start_date = data['start_date']
        end_date = data['end_date']
        domain += [('date_planned', '>=', start_date), ('date_planned', '<=', end_date)]

        records = self.env['mrp.production'].search(domain)

        res = {
            'data': data,
            'records': records
        }

        return self.env['report'].render('production.report_production_status', res)
