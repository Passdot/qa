# -*- coding: utf-8 -*-

from openerp import api, models


class ProductionInventory(models.AbstractModel):
    _name = 'report.production.report_mrp_inventory'
    _description = 'Report MRP Inventory'

    @api.multi
    def render_html(self, data):
        domain = []  # [('state', '=', 'done')]

        # Products
        product_ids = data['product_ids']
        if product_ids:
            domain += [('product_id', 'in', product_ids)]

        # Locations
        dest_ids = data['location_ids']
        if dest_ids:
            domain += [('location_id', 'in', dest_ids)]

        # Category
        categ_ids = data['category_ids']
        if categ_ids:
            domain += [('product_id.categ_id', 'in', categ_ids)]

        # Batches/Production
        mo_ids = data['mo_ids']
        if mo_ids:
            domain += [('history_ids.production_id', '=', mo_ids)]

        # Start - End DATE
        start_date = data['start_date']
        end_date = data['end_date']
        domain += [('in_date', '>=', start_date), ('in_date', '<=', end_date)]

        records = self.env['stock.quant'].search(domain)

        recs = []
        for rec in records:
            production_names = rec.history_ids.mapped('production_id.name')
            references = rec.history_ids.mapped('origin')
            recs.append({
                'batch': production_names and production_names[0] or references and references[0] or '',
                'product': rec.product_id.display_name,
                'qty': rec.qty,
                'uom': rec.product_id.uom_id.name,
                'category': rec.product_id.categ_id.name,
                'inventory_value': rec.inventory_value,
            })

        res = {
            'data': data,
            'records': recs
        }

        return self.env['report'].render('production.report_mrp_inventory', res)
