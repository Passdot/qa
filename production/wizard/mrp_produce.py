# -*- coding: utf-8 -*-


from openerp import api, fields, models, _
from openerp.tools import float_is_zero
from openerp.exceptions import except_orm


class Produce(models.TransientModel):
    _inherit = "mrp.product.produce"

    def _get_default_production(self):
        return self.env.context.get('active_id')

    def _get_default_workcenter_id(self):
        production_id = self._get_default_production()
        workcenter = self.env['mrp.production.workcenter.line'].search(
            [('state', '=', 'done'), ('production_id', '=', production_id)],
            order='sequence desc', limit=1)
        return workcenter

    mode = fields.Selection(default="consume")
    workcenter_id = fields.Many2one('mrp.production.workcenter.line', string="Workcenter Line", readonly=True, default=_get_default_workcenter_id)
    production_id = fields.Many2one('mrp.production', string="Production", default=_get_default_production)

    @api.multi
    def do_produce(self):
        self.ensure_one()
        context = dict(self.env.context)
        production_id = context.get('active_id', False)
        assert production_id, "Production Id should be specified in context as a Active ID."
        production = self.env['mrp.production'].browse(production_id)
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        StockMove = self.env['stock.move']

        if context.get('restart'):
            if self.production_id.workcenter_lines.filtered(lambda rec: rec.state in ('pause', 'startworking')):
                raise except_orm(
                        _('Could not restart !'),
                        _("You cannot restart any workorder when there are any process is still in running or paused state."))
            new_workorder = self.workcenter_id.copy()
            new_workorder.is_restarted = True
            new_workorder.move_dest_id = self.workcenter_id.move_dest_id.id
            self.workcenter_id.action_cancel()

            # Re-Assign all consuption to this Order
            for consume in self.consume_lines:
                if consume.product_id.qty_available < consume.product_qty:
                    raise except_orm(
                        _('No Quantity !'),
                        _("There are not enough quantity to proceed."))
                remaining_qty = consume.product_qty
                if not float_is_zero(remaining_qty, precision_digits=precision):
                    extra_move_id = production._make_consume_line_from_data(
                        production, consume.product_id, consume.product_id.uom_id.id, remaining_qty, False, 0)
                    extra_move = StockMove.browse(extra_move_id)
                    extra_move.write({
                        'restrict_lot_id': consume.lot_id,
                        'consumed_for': new_workorder.move_dest_id.id
                    })
                    extra_move.action_done()
            new_workorder.signal_workflow('button_start_working')
        else:
            self.env['mrp.production'].action_produce(production_id, self.product_qty, self.mode, self)

        return {}
