# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


class QualityParameterInput(models.TransientModel):
    """
        QUality Verification Input
        Copy QA Form parameters, allow just value field
    """
    _name = "quality.wizard.verification.workorder.input"
    _description = "Quality Verification Workorder Input"

    name = fields.Char(required=True, readonly=True)
    value_type = fields.Selection([('range', 'Range'), ('boolean', 'Boolean'), ('remark', 'Remark')],
                                  string="Type", default='range', required=True, readonly=True)
    range_from = fields.Float("Range From", readonly=True)
    range_to = fields.Float("Range To", readonly=True)
    bool_field = fields.Boolean('Exist ?', help="Check it if Yes")
    remark = fields.Char("Remark")

    uom_id = fields.Many2one('product.uom', string='Unit of Measure', readonly=True)
    error = fields.Boolean(help='Incorrect Value', readonly=True)

    qa_verify_id = fields.Many2one('quality.wizard.verification.workorder', 'Verification Report')
    qa_form_id = fields.Many2one(related='qa_verify_id.qa_form_id', string="Form", readonly=True)

    range_value = fields.Float()
    bool_value = fields.Boolean("Boolean Value")


class QualityVerification(models.TransientModel):
    _name = 'quality.wizard.verification.workorder'
    _description = 'Quality Verification Work Order'

    name = fields.Char("Report Name")
    description = fields.Text()

    line_id = fields.Many2one('mrp.production.workcenter.line', string="Workcenter Line")
    product_id = fields.Many2one('product.product', string='Product')
    qa_form_id = fields.Many2one(related='product_id.qa_form_id', string="QA Form")
    quantity = fields.Float()
    param_ids = fields.One2many('quality.wizard.verification.workorder.input', 'qa_verify_id', string="Parameters")
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending')],
                                      string='QA Test Result', default='pending')
    result_desc = fields.Text('Failure Reason')

    @api.multi
    def validate_qa_form(self):
        self.ensure_one()
        param_vals = []
        for param in self.param_ids:
            param_vals.append((0, 0, {
                'name': param.name,
                'value_type': param.value_type,
                'range_from': param.range_from,
                'range_to': param.range_to,
                'uom_id': param.uom_id.id,
                'bool_field': param.bool_field,
                'remark': param.remark,
                'range_value': param.range_value,
                'bool_value': param.bool_value,
                'error': param.error,
            }))
            if param.value_type == 'range':
                if param.range_value < param.range_from or param.range_value > param.range_to:
                    param.error = True
            elif param.value_type == 'boolean':
                if param.bool_field != param.bool_value:
                    param.error = True
        if self.param_ids.filtered('error'):
            self.qa_test_result = 'fail'
            self.result_desc = ', '.join(self.param_ids.filtered('error').mapped('name')) + ' has incorrect values'
        else:
            self.qa_test_result = 'success'
            self.result_desc = 'Success'

        self.line_id.qa_test_result = self.qa_test_result
        self.line_id.action_done()

        self.env['quality.verification'].create({
            'name': self.name,
            'result_type': 'mo',
            'quantity': self.line_id.output_qty,
            'mo_id': self.line_id.production_id.id,
            'workcenter_id': self.line_id.id,
            'product_id': self.product_id.id,
            'qa_form_id': self.qa_form_id.id,
            'qa_test_result': self.qa_test_result,
            'result_desc': self.result_desc,
            'param_ids': param_vals
        })

        view = self.env.ref('production.quality_wizard_verification_workorder_form_view_readonly_result')
        return {
             'name': _('Quality Check Result'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'quality.wizard.verification.workorder',
             'views': [(view.id, 'form')],
             'view_id': view.id,
             'target': 'new',
             'res_id': self.id,
         }
