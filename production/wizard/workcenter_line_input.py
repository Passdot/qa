# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models


class WorkcenterLineInput(models.TransientModel):
    _name = 'mrp.production.workcenter.line.input'
    _description = "Workcenter Line Input"

    output_qty = fields.Integer("Produced Quntity", help="Output Quantity")
    line_id = fields.Many2one('mrp.production.workcenter.line', string="Workcenter Line")

    @api.multi
    def action_write_output_qty(self):
        self.ensure_one()
        # Writeout Output QTY
        line = self.line_id
        line.output_qty = self.output_qty
        line.move_dest_id.product_uom_qty = line.output_qty
        line.action_done()
