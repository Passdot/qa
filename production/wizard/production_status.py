# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import except_orm


class ProductionInventory(models.TransientModel):
    _name = 'production.status'
    _description = 'Report Production Status'

    all_batch = fields.Boolean('All Batch', default=True)

    all_new = fields.Boolean('All New Batch')
    all_cancel = fields.Boolean('All Cancelled Batch')
    all_confirmed = fields.Boolean('All Confirmed Batch')
    all_ready = fields.Boolean('All Ready Batch')
    all_in_production = fields.Boolean('All In Progress Batch')
    all_done = fields.Boolean('All Completed Batch')

    start_date = fields.Date('Start Date', required=True, default=fields.Date.today, help="Scheduled planned start date")
    end_date = fields.Date('End Date', required=True, default=fields.Date.today, help="Scheduled planned end date")

    @api.onchange("all_batch")
    def _onchange_all_batch(self):
        if self.all_batch:
            self.all_cancel = True
            self.all_confirmed = True
            self.all_ready = True
            self.all_in_production = True
            self.all_done = True
        else:
            self.all_cancel = False
            self.all_confirmed = False
            self.all_ready = False
            self.all_in_production = False
            self.all_done = False

    @api.multi
    def print_report(self):
        self.ensure_one()
        if not any([self.all_batch, self.all_cancel, self.all_confirmed, self.all_ready, self.all_in_production, self.all_done]):
            raise except_orm(
                _('No Filters !'),
                _("Kindly select any of the above filter to print the report."))
        data = self.read()[0]
        return self.env['report'].get_action(self, 'production.report_production_status', data=data)
