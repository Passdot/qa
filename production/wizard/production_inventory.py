# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import except_orm


class ProductionInventory(models.TransientModel):
    _name = 'production.inventory'
    _description = 'Report Production Inventory'

    product_ids = fields.Many2many('product.product', 'product_mrp_inventory_rel', string='Finished Products')
    all_product = fields.Boolean('Overall Products')

    location_ids = fields.Many2many('stock.location', string='Destination Location')
    all_location = fields.Boolean('All Locations')

    category_ids = fields.Many2many('product.category', string='Category')
    all_category = fields.Boolean('All Categories')

    mo_ids = fields.Many2many('mrp.production', string='Batches', domain="[('state', '=', 'done')]")
    all_mo = fields.Boolean('All Batches')

    start_date = fields.Date('Start Date', required=True, default=fields.Date.today)
    end_date = fields.Date('End Date', required=True, default=fields.Date.today)

    @api.onchange("all_product")
    def _onchange_all_product(self):
        if self.all_product:
            mos = self.env['mrp.production'].search([('state', '=', 'done')])
            self.product_ids = mos.mapped('product_id')
        else:
            self.product_ids = False

    @api.onchange("all_location")
    def _onchange_all_location(self):
        if self.all_location:
            self.location_ids = self.env['stock.location'].search([])
        else:
            self.location_ids = False

    @api.onchange("all_category")
    def _onchange_all_category(self):
        if self.all_category:
            self.category_ids = self.env['product.category'].search([])
        else:
            self.category_ids = False

    @api.onchange("all_mo")
    def _onchange_all_mo(self):
        if self.all_mo:
            self.mo_ids = self.env['mrp.production'].search([('state', '=', 'done')])
        else:
            self.mo_ids = False

    @api.multi
    def print_report(self):
        self.ensure_one()
        if not any([self.product_ids, self.location_ids, self.category_ids, self.mo_ids]):
            raise except_orm(
                _('No Filters !'),
                _("Kindly select any of the above filter to print the report."))
        data = self.read()[0]
        return self.env['report'].get_action(self, 'production.report_mrp_inventory', data=data)
