# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
from openerp.exceptions import except_orm


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    todo_qa_test = fields.Boolean(
        related="workcenter_lines.todo_qa_test",
        string="Todo QA Test after this ?", help="Check this if you want QA testing after this completing this work order")
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      related="workcenter_lines.qa_test_result",
                                      string='QA Test Result', default='na', copy=False)

    # @api.depends('todo_qa_test', 'workcenter_lines', 'workcenter_lines.qa_test_result')
    # def _compute_qa_test_result(self):
    #     for production in self:
    #         if not production.workcenter_lines or production.workcenter_lines.filtered(lambda rec: not rec.todo_qa_test):
    #             production.qa_test_result = 'na'
    #         else:
    #             production.qa_test_result = production.workcenter_lines[-1].qa_test_result

    def action_production_end(self):
        procs = self.env["procurement.order"].search([('production_id', 'in', self.ids)])
        procs.check()
        return True

    @api.multi
    def action_set_production_done(self):
        self.ensure_one()
        for production in self:
            self._costs_generate(production)
        write_res = self.write({'state': 'done', 'date_finished': fields.Datetime.now()})
        return write_res

    @api.model
    def _make_production_produce_line(self, production):
        StockMove = self.env['stock.move']
        Procurement = self.env['procurement.order']
        source_location_id = production.product_id.property_stock_production.id
        destination_location_id = production.location_dest_id.id
        procurement = Procurement.search([('production_id', '=', production.id)], limit=1)
        res = False
        for line in production.workcenter_lines:
            vals = {
                'name': production.name,
                'date': production.date_planned,
                'product_id': line.product_id.id,
                'product_uom': line.uom_id.id,
                'product_uom_qty': line.expected_qty,
                'location_id': source_location_id,
                'location_dest_id': destination_location_id,
                'procurement_id': procurement.id,
                'company_id': production.company_id.id,
                'production_id': production.id,
                'origin': production.name,
                'group_id': procurement.group_id.id,
            }
            move = StockMove.create(vals)
            line.move_dest_id = move.id
            res = move.action_confirm()
        return res


class Workcenter(models.Model):
    _inherit = 'mrp.routing.workcenter'

    todo_qa_test = fields.Boolean("Todo QA Test after this ?", help="Check this if you want QA testing after this completing this work order")
    qa_form_id = fields.Many2one('quality.form', string='Quality Form')
    product_id = fields.Many2one('product.product', string='Output Product')
    expected_qty = fields.Integer('Expected QTY', required=True)
    uom_id = fields.Many2one(related="product_id.uom_id")

    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.todo_qa_test = self.product_id.todo_qa_test
        self.qa_form_id = self.product_id.qa_form_id


class MrpProductionWorkcenterLine(models.Model):
    _inherit = 'mrp.production.workcenter.line'

    todo_qa_test = fields.Boolean("Todo QA Test after this ?", help="Check this if you want QA testing after this completing this work order")
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      string='QA Test Result', default='na', copy=False, store=True, compute="_compute_qa_test_result")
    product_id = fields.Many2one('product.product', string='Output Product')
    qa_form_id = fields.Many2one('quality.form', string='Quality Form')
    uom_id = fields.Many2one(related="product_id.uom_id")
    output_qty = fields.Integer('Output QTY', help="After finishing this step output product which is input for the next", copy=False)
    expected_qty = fields.Integer('Expected QTY', copy=False)
    is_first = fields.Boolean('Is first line ?', compute="_compute_is_first")
    is_last = fields.Boolean('Is last line ?', compute="_compute_is_first")
    move_dest_id = fields.Many2one('stock.move', 'Destination Move', help="Move which caused (created) the procurement")
    is_restarted = fields.Boolean('Is Restarted')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.todo_qa_test = self.product_id.todo_qa_test
        self.qa_form_id = self.product_id.qa_form_id
        self.qa_test_result = 'pending'

    @api.depends('todo_qa_test', 'product_id')
    def _compute_qa_test_result(self):
        for rec in self:
            rec.qa_test_result = rec.todo_qa_test and 'pending' or 'na'

    def _compute_is_first(self):
        for line in self:
            workcenter_lines = self.search([('production_id', '=', line.production_id.id)], order="sequence")
            workcenter_lines[0].is_first = True
            workcenter_lines[-1].is_last = True

    @api.multi
    def action_start_working(self):
        # Check quantity
        for move in self.mapped('production_id').mapped('move_lines'):
            if move.product_qty > move.product_id.qty_available:
                raise except_orm(
                    _('No Quantity !'),
                    _("There are not enough quantity to proceed."))

        # Check previous work order is in done stage and QA done if required.
        for line in self:
            seq = line.sequence
            mv_lines = line.production_id.workcenter_lines.filtered(lambda mv: mv.id != line.id and mv.sequence < seq and mv.state not in ['done', 'cancel'])

            # Previous Done ?
            if mv_lines:
                raise except_orm(
                    _('Waiting !'),
                    _("You cannot proceed for the next step because make sure previous work is in done state."))

            # QA Test if Yes then Done ?
            mv_lines = line.production_id.workcenter_lines.filtered(
                lambda mv: mv.id != line.id and mv.sequence < seq and mv.state == 'done' and mv.todo_qa_test and mv.qa_test_result != 'success')
            if mv_lines:
                raise except_orm(
                    _('QA Required !'),
                    _("You cannot proceed for the next step because previous work order needs QA testing."))
        res = super(MrpProductionWorkcenterLine, self).action_start_working()

        StockMove = self.env['stock.move']
        for line in self.filtered(lambda rec: not line.is_restarted):
            if line.is_first:
                continue
            prev_line = self.search([('sequence', '<', line.sequence), ('production_id', '=', line.production_id.id), ('state', '=', 'done')],
                                    order="sequence desc", limit=1)
            if prev_line:
                production = line.production_id
                move_id = production._make_consume_line_from_data(
                        production, prev_line.product_id, prev_line.product_id.uom_id.id, prev_line.output_qty, False, 0)
                move = StockMove.browse(move_id)
                move.action_confirm()
                move.action_done()

        return res

    @api.multi
    def action_done(self):
        for line in self:
            # Whether line has QA Test ? if yes, then done it only if result is 'success'
            # Otherwize just make LINE -> DONE, BUT NOT MOVE
            if not line.todo_qa_test or (line.todo_qa_test and line.qa_test_result == 'success'):
                if line.is_last:
                    line.production_id.product_qty = line.output_qty
                if line.move_dest_id.state == 'draft':
                    line.move_dest_id.action_confirm()
                line.move_dest_id.action_done()
        res = super(MrpProductionWorkcenterLine, self).action_done()
        for line in self:
            if not line.todo_qa_test or (line.todo_qa_test and line.qa_test_result == 'success'):
                if line.is_last:
                    line.production_id.action_set_production_done()
        return res

    @api.multi
    def action_done_open_output_wizard(self):
        # super(MrpProductionWorkcenterLine, self).action_done()
        view = self.env.ref('production.wizard_workcenter_line_input_form_view')
        wizard = self.env['mrp.production.workcenter.line.input'].create({
                'line_id': self.id,
            })
        return {
             'name': _('Output Quantity'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'mrp.production.workcenter.line.input',
             'views': [(view.id, 'form')],
             'view_id': view.id,
             'target': 'new',
             'res_id': wizard.id,
        }

    @api.multi
    def action_open_form(self):
        self.ensure_one()
        param_vals = []
        for param in self.qa_form_id.parameter_ids:
            param_vals.append((0, 0, {
                'name': param.name,
                'value_type': param.value_type,
                'range_from': param.range_from,
                'range_to': param.range_to,
                'uom_id': param.uom_id.id,
                'bool_field': param.bool_field,
                'remark': param.remark
            }))
        vals = {
            'name': 'QTY/%s/%s' % (self.production_id.name, self.name),
            'line_id': self.id,
            'quality': self.output_qty,
            'product_id': self.product_id.id,
            'param_ids': param_vals,
        }

        verification = self.env['quality.wizard.verification.workorder'].create(vals)
        view = self.env.ref('production.quality_wizard_verification_workorder_form_view')

        ctx = dict(self.env.context)
        ctx.update({
            'this_packop_id': self.id,
        })
        return {
             'name': _('Quality Verfication Input'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'quality.wizard.verification.workorder',
             'views': [(view.id, 'form')],
             'view_id': view.id,
             'target': 'new',
             'res_id': verification.id,
             'context': ctx,
        }
