# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Quality Assurance',
    'version': '1',
    'summary': 'Quality Assurance',
    'sequence': 30,
    'category': 'Stock',
    'depends': [
        'stock', 'purchase'
    ],
    'data': [
        'views/quality_views.xml',
        'views/product_views.xml',
        'views/stock_views.xml',
        'report/report_qa_templates.xml',
        'wizard/quality_verification_views.xml',
        'wizard/stock_transfer_views.xml',
    ],
    'demo': [
    ],
    'installable': True,
}
