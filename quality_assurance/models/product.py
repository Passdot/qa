# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import fields, models


class Template(models.Model):
    _inherit = "product.template"

    todo_qa_test = fields.Boolean("Todo QA Test ?", help="Check this if you want QA testing of this product")
    qa_form_id = fields.Many2one('quality.form', string='Quality Form')
