# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    is_qa_required = fields.Boolean(compute='_compute_is_qa_required', string="QA Required to any product ?", store=True)
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      compute="_compute_qa_test_result", string='QA Test Result', default='na')

    @api.depends('move_lines.product_id')
    def _compute_is_qa_required(self):
        for picking in self:
            picking.is_qa_required = any(picking.mapped('move_lines.product_id.todo_qa_test'))

    def _compute_qa_test_result(self):
        verifs = self.env['quality.verification'].search([('picking_id', 'in', self.ids)])
        for picking in self:
            test_result = verifs.filtered(lambda ver: ver.picking_id.id == picking.id).mapped('qa_test_result')
            picking.qa_test_result = 'na'
            if not test_result and picking.is_qa_required or 'pending' in test_result:
                picking.qa_test_result = 'pending'
            elif 'success' in test_result:
                picking.qa_test_result = 'success'
            elif 'fail' in test_result:
                picking.qa_test_result = 'fail'


class StockMove(models.Model):
    _inherit = 'stock.move'

    # @api.depends('product_id')
    def _compute_qa_test_result(self):
        for move in self:
            # move.is_qa_required = move.product_id.todo_qa_test and 'pending' or 'na'
            return move.product_id.todo_qa_test and 'pending' or 'na'

    is_qa_required = fields.Boolean(related='product_id.todo_qa_test', string="QA Required to any product ?", store=True)
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      default=_compute_qa_test_result, string='QA Test Result')
