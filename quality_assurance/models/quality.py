# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import fields, models


class QualityParameters(models.Model):
    """
        QUality Parameters
    """
    _name = "quality.parameter"
    _description = "Quality Parameters"

    name = fields.Char(required=True)
    value_type = fields.Selection([('range', 'Range'), ('boolean', 'Boolean'), ('remark', 'Remark')],
                                  string="Type", default='range', required=True)
    range_from = fields.Float("Range From")
    range_to = fields.Float("Range To")
    bool_field = fields.Boolean('Value should be ?', help="Check it if Yes")
    remark = fields.Char("Remark")
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')

    qa_form_id = fields.Many2one("quality.form", string="Form")


class Quality(models.Model):
    """
        QUality Assurance
    """
    _name = "quality.form"
    _description = "Quality Form"

    name = fields.Char(required=True)
    description = fields.Text()

    parameter_ids = fields.One2many('quality.parameter', 'qa_form_id', string="Parameters")


class QualityParameterInput(models.Model):
    """
        QUality Verification Input
        Copy QA Form parameters, allow just value field
    """
    _name = "quality.verification.input"
    _description = "Quality Verification Input"

    name = fields.Char(required=True, readonly=True)
    value_type = fields.Selection([('range', 'Range'), ('boolean', 'Boolean'), ('remark', 'Remark')],
                                  string="Type", default='range', required=True, readonly=True)
    range_from = fields.Float("Range From", readonly=True)
    range_to = fields.Float("Range To", readonly=True)
    bool_field = fields.Boolean('Boolean Field', readonly=True)
    remark = fields.Char("Remark")

    uom_id = fields.Many2one('product.uom', string='Unit of Measure', readonly=True)
    error = fields.Boolean(help='Incorrect Value', readonly=True)

    qa_verify_id = fields.Many2one('quality.verification', 'Verification Report')
    qa_form_id = fields.Many2one(related='qa_verify_id.qa_form_id', string="Form", readonly=True)

    range_value = fields.Float()
    bool_value = fields.Boolean("Boolean Value")


class QualityVerification(models.Model):
    _name = 'quality.verification'
    _description = 'Quality Verification'

    name = fields.Char("Report Name")
    # description = fields.Text()

    result_type = fields.Selection([('po', 'Purchase'), ('mo', 'Manufacturing')])

    # PO
    packop_id = fields.Many2one('stock.pack.operation', string='Pack Operation ID', readonly="1")
    # packop_id = fields.Integer(string='Pack Operation ID', readonly="1")
    picking_id = fields.Many2one('stock.picking', string='Picking')
    po_origin = fields.Char(related='picking_id.origin', string='Purchase Order')

    # MO
    workcenter_id = fields.Many2one('mrp.production.workcenter.line', string="Workcenter Line")
    mo_id = fields.Many2one(related='workcenter_id.production_id', string='Manufacturing Order')

    product_id = fields.Many2one('product.product', string='Product')
    qa_form_id = fields.Many2one(related='product_id.qa_form_id', string="QA Form")

    quantity = fields.Float()
    param_ids = fields.One2many('quality.verification.input', 'qa_verify_id', string="Parameters")
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      string='QA Test Result', default='na')
    result_desc = fields.Text('Failure Reason')
