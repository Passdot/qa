# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


class QualityParameterInput(models.TransientModel):
    """
        QUality Verification Input
        Copy QA Form parameters, allow just value field
    """
    _name = "quality.wizard.verification.input"
    _description = "Quality Verification Input"

    name = fields.Char(required=True, readonly=True)
    value_type = fields.Selection([('range', 'Range'), ('boolean', 'Boolean'), ('remark', 'Remark')],
                                  string="Type", default='range', required=True, readonly=True)
    range_from = fields.Float("Range From", readonly=True)
    range_to = fields.Float("Range To", readonly=True)
    bool_field = fields.Boolean('Exist ?', help="Check it if Yes")
    remark = fields.Char("Remark")

    uom_id = fields.Many2one('product.uom', string='Unit of Measure', readonly=True)
    error = fields.Boolean(help='Incorrect Value', readonly=True)

    qa_verify_id = fields.Many2one('quality.wizard.verification', 'Verification Report')
    qa_form_id = fields.Many2one(related='qa_verify_id.qa_form_id', string="Form", readonly=True)

    range_value = fields.Float()
    bool_value = fields.Boolean("Boolean Value")


class QualityVerification(models.TransientModel):
    _name = 'quality.wizard.verification'
    _description = 'Quality Verification'

    name = fields.Char("Report Name")
    # transfer_id = fields.Many2one('stock.transfer_details', 'Transfer')
    # item_id = fields.Many2one('stock.transfer_details_items', 'Items')
    picking_id = fields.Many2one('stock.picking', string='Picking')
    product_id = fields.Many2one('product.product', string='Product')
    qa_form_id = fields.Many2one(related='product_id.qa_form_id', string="QA Form")
    quantity = fields.Float()
    param_ids = fields.One2many('quality.wizard.verification.input', 'qa_verify_id', string="Parameters")
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending')],
                                      string='QA Test Result', default='pending')
    result_desc = fields.Text('Failure Reason')

    @api.multi
    def validate_qa_form(self):
        self.ensure_one()
        param_vals = []
        for param in self.param_ids:
            param_vals.append((0, 0, {
                'name': param.name,
                'value_type': param.value_type,
                'range_from': param.range_from,
                'range_to': param.range_to,
                'uom_id': param.uom_id.id,
                'bool_field': param.bool_field,
                'remark': param.remark,
                'range_value': param.range_value,
                'bool_value': param.bool_value,
                'error': param.error,
            }))
            if param.value_type == 'range':
                if param.range_value < param.range_from or param.range_value > param.range_to:
                    param.error = True
            elif param.value_type == 'boolean':
                if param.bool_field != param.bool_value:
                    param.error = True
        if self.param_ids.filtered('error'):
            self.qa_test_result = 'fail'
            self.result_desc = ', '.join(self.param_ids.filtered('error').mapped('name')) + ' has incorrect values'
        else:
            self.qa_test_result = 'success'
            self.result_desc = 'Success'

        # self.picking_id.qa_test_result = self.qa_test_result

        verif = self.env['quality.verification'].create({
            'name': self.name,
            'result_type': 'po',
            'picking_id': self.picking_id.id,
            'packop_id': self.env.context.get('this_packop_id'),
            'quantity': self.quantity,
            'product_id': self.product_id.id,
            'qa_form_id': self.qa_form_id.id,
            'qa_test_result': self.qa_test_result,
            'result_desc': self.result_desc,
            'param_ids': param_vals
        })

        view = self.env.ref('quality_assurance.quality_verification_form_view_wizard')

        ctx = dict(self.env.context)
        ctx.update({
            'this_packop_id': verif.packop_id.id,
            'this_verif_id': verif.id,
        })
        return {
             'name': _('Quality Check Result'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'quality.wizard.verification',
             'views': [(view.id, 'form')],
             'view_id': view.id,
             'target': 'new',
             'res_id': self.id,
             'context': ctx,
         }

    @api.multi
    def open_transfer_wizard(self):
        self.ensure_one()
        # self.picking_id.do_enter_transfer_details()
        context = dict(self.env.context)
        context.update({
            'active_ids': self.picking_id.ids,
            'active_id': self.picking_id.id,
            'active_model': 'stock.picking',
        })
        vals = {
            'picking_id': self.picking_id.id,
            'qa_test_result': self.qa_test_result,
        }
        wizard = self.env['stock.transfer_details'].with_context(context).create(vals)
        return wizard.wizard_view()

    @api.multi
    def cancel(self):
        self.ensure_one()
        context = dict(self.env.context)
        context.update({
            'active_ids': self.picking_id.ids,
            'active_id': self.picking_id.id,
            'active_model': 'stock.picking',
        })
        wizard = self.env['stock.transfer_details'].create({'picking_id': self.picking_id.id})
        return wizard.with_context(context).wizard_view()
