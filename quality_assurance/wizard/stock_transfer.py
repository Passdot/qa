# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
from openerp.exceptions import Warning


class StockTransfer(models.TransientModel):
    """
        Stock Transfer
    """

    _inherit = 'stock.transfer_details'

    is_qa_required = fields.Boolean(compute='_compute_is_qa_required', string="QA Required to any product ?", store=True)
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending'), ('na', 'Not Applicable')],
                                      compute="_compute_qa_test_result", string='QA Test Result', default='na')

    @api.depends('item_ids.product_id')
    def _compute_is_qa_required(self):
        for transfer in self:
            transfer.is_qa_required = any(transfer.mapped('item_ids.product_id.todo_qa_test'))

    @api.depends('is_qa_required', 'item_ids.qa_test_result')
    def _compute_qa_test_result(self):
        qa_req_count = 0
        qa_test_success = 0
        for item in self.mapped('item_ids'):
            if item.todo_qa_test:
                qa_req_count += 1
            if item.qa_test_result == 'success':
                qa_test_success += 1

            if qa_req_count == qa_test_success:
                item.transfer_id.qa_test_result = 'success'
            else:
                item.transfer_id.qa_test_result = item.todo_qa_test and 'pending' or 'na'

    @api.model
    def default_get(self, field_list):
        res = super(StockTransfer, self).default_get(field_list)
        Verification = self.env['quality.verification']
        context = dict(self.env.context)

        picking_ids = context.get('active_ids', [])
        active_model = context.get('active_model')

        if not picking_ids or len(picking_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        assert active_model in ('stock.picking'), 'Bad context propagation'
        picking_id, = picking_ids
        picking = self.env['stock.picking'].browse(picking_id)

        verifs = Verification.search([
                ('packop_id', 'in', picking.pack_operation_ids.ids)
            ])

        items = []
        packs = []
        if not picking.pack_operation_ids:
            picking.do_prepare_partial()
        for op in picking.pack_operation_ids:
            item = {
                'packop_id': op.id,
                'product_id': op.product_id.id,
                'product_uom_id': op.product_uom_id.id,
                'quantity': op.product_qty,
                'package_id': op.package_id.id,
                'lot_id': op.lot_id.id,
                'sourceloc_id': op.location_id.id,
                'destinationloc_id': op.location_dest_id.id,
                'result_package_id': op.result_package_id.id,
                'date': op.date,
                'owner_id': op.owner_id.id,
            }
            if op.id in verifs.mapped('packop_id').ids:
                test_result = verifs.filtered(lambda ver: ver.packop_id.id == op.id).mapped('qa_test_result')
                item['qa_test_result'] = 'na'
                if not test_result and picking.is_qa_required or 'pending' in test_result:
                    item['qa_test_result'] = 'pending'
                elif 'success' in test_result:
                    item['qa_test_result'] = 'success'
                elif 'fail' in test_result:
                    item['qa_test_result'] = 'fail'
            if op.product_id:
                items.append(item)
            elif op.package_id:
                packs.append(item)
        res.update(item_ids=items)
        res.update(packop_ids=packs)
        return res

    @api.one
    def do_detailed_transfer(self):
        if self.is_qa_required and self.qa_test_result != 'success':
            raise Warning(_('There are some products needs QA testing.'))
        return super(StockTransfer, self).do_detailed_transfer()


class StockTransferItems(models.TransientModel):
    _inherit = 'stock.transfer_details_items'

    todo_qa_test = fields.Boolean(related='product_id.todo_qa_test')
    qa_test_result = fields.Selection([('success', 'Success'), ('fail', 'Fail'), ('pending', 'Pending')],
                                      string='QA Test Result', default='pending')

    @api.multi
    def open_validation_form(self):
        self.ensure_one()

        param_vals = []
        for param in self.product_id.qa_form_id.parameter_ids:
            param_vals.append((0, 0, {
                'name': param.name,
                'value_type': param.value_type,
                'range_from': param.range_from,
                'range_to': param.range_to,
                'uom_id': param.uom_id.id,
                'bool_field': param.bool_field,
                'remark': param.remark
            }))
        vals = {
            'name': 'QTY/%s/%s' % (self.transfer_id.picking_id.origin, self.transfer_id.picking_id.name),
            'picking_id': self.transfer_id.picking_id.id,
            'product_id': self.product_id.id,
            'quantity': self.quantity,
            'param_ids': param_vals,
        }

        verification = self.env['quality.wizard.verification'].create(vals)
        view = self.env.ref('quality_assurance.quality_verification_form_view')

        ctx = dict(self.env.context)
        ctx.update({
            'this_packop_id': self.packop_id.id,
        })
        return {
             'name': _('Quality Verfication Input'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'quality.wizard.verification',
             'views': [(view.id, 'form')],
             'view_id': view.id,
             'target': 'new',
             'res_id': verification.id,
             'context': ctx,
        }
